#!/bin/env python
import urllib2
import re
import os
from subprocess import Popen
from distutils.version import LooseVersion


class PHPMyAdmin():
    def __init__(self, path, user):
        self.path = path
        self.version = self._current_version()
        self.user = user
        self.delete_all = True

    def _current_version(self):
        try:
            with open(self.path + '/ChangeLog', 'r') as version_file:
                match = re.search('((?:(?:[0-9])+\.)+[0-9]+)\s\([0-9]+\-[0-9]+\-[0-9]+\)', version_file.read())
                version = match.group(1)
                return version
        except:
            raise Exception('phpmyadmin not found')

    @staticmethod
    def _parse_links(html):
        updates = re.search('/phpmyadmin/phpmyadmin/archive/RELEASE_((?:(?:[0-9])+\_)+[0-9]+).zip', str(html))
        try:
            update_url = 'https://github.com' + updates.group(0)
            version = updates.group(1).replace('_','.')
        except:
            return None, None
        return update_url, version

    def check_update(self):
        html, err = self._request('https://github.com/phpmyadmin/phpmyadmin/releases')
        if not html:
            return False
        update_url, new_version = self._parse_links(html)
        if LooseVersion(new_version) > LooseVersion(self.version):
            if self._update(update_url, new_version):
                self.version = new_version
        return True

    def _update(self, url, version):
        update_file, err = self._request(url)
        if not update_file:
            return False
        with open('/tmp/phpmyadmin_update.zip', 'wb') as zip_file:
            zip_file.write(update_file)

        if not os.path.isdir('/tmp/phpmyadmin_' + version):
            os.mkdir('/tmp/phpmyadmin_' + version)
        # unzip
        p = Popen(['unzip', '-q', '-o', '/tmp/phpmyadmin_update.zip', '-d' '/tmp/phpmyadmin_' + version])
        p.wait()
        os.remove('/tmp/phpmyadmin_update.zip')
        # copy files
        try:
            if self.delete_all:
                p = Popen(['rm -rf ' + self.path + '/!config.inc.php'], shell=True)
                p.wait()
            p = Popen(
                ['cp -rf /tmp/phpmyadmin_' + version + '/phpmyadmin-RELEASE*/* ' + self.path +
                 ' && chown -hR  ' + self.user + ':' + self.user + ' ' + self.path +
                 ' && rm -rf ' + '/tmp/phpmyadmin_' + version], shell=True)
            p.wait()
        except:
            raise Exception('no permission to write')
        self.version = version
        return True

    #
    @staticmethod
    def _request(url, timeout=5):
        try:
            req = urllib2.Request(url, headers={
                'User-Agent': 'lel',
            })
            resp = urllib2.urlopen(req, timeout=timeout)
            html = resp.read()
            resp.close()
            return html, False
        except urllib2.HTTPError as e:
            return False, e.code
        except:
            return False, False
