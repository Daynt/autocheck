#!/usr/bin python
import urllib2
import re
import os
from subprocess import Popen


class Pydio():
    def __init__(self, path, user):
        self.path = path
        self.version = self._current_version()
        self.user = user

    def _current_version(self):
        try:
            with open(self.path + '/conf/VERSION', 'r') as version_file:
                return version_file.read().split('__')[0]
        except:
            raise Exception('pydio not found')

    def _parse_links(self, html):
        updates = re.search(
            'http:/\/sourceforge.net\/projects\/ajaxplorer\/files\/pydio\/stable-channel\/(?:[0-9]+?\.)+'
            '[0-9]+\/pydio-core-upgrade-' + self.version + '-((?:(?:[0-9])+\.)+[0-9]+)\.zip\/download', str(html))
        try:
            update_url = updates.group(0)
            version = updates.group(1)
        except:
            return None, None
        return update_url, version

    def check_update(self):
        html, err = self._request('http://sourceforge.net/projects/ajaxplorer/rss/?path=/pydio/stable-channel/')
        if not html:
            return False

        update_url, new_version = self._parse_links(html)
        if not new_version:
            return False
        while new_version:
            self._update(update_url, new_version)
            update_url, new_version = self._parse_links(html)
        return True

    def _update(self, url, version):
        update_file, err = self._request(url)
        if not update_file:
            return False
        with open('/tmp/pydio_update.zip', 'wb') as zip_file:
            zip_file.write(update_file)

        if not os.path.isdir('/tmp/pydio_' + version):
            os.mkdir('/tmp/pydio_' + version)
        # unzip
        p = Popen(['unzip', '-q', '-o', '/tmp/pydio_update.zip', '-d' '/tmp/pydio_' + version])
        p.wait()
        os.remove('/tmp/pydio_update.zip')
        # copy files
        try:
            p = Popen(['cp -rf /tmp/pydio_' + version + '/pydio*/* ' + self.path +
                       ' && chown -hR  ' + self.user + ':' + self.user + ' ' + self.path +
                       ' && rm -rf ' + '/tmp/pydio_' + version], shell=True)
            p.wait()
        except:
            raise Exception('no permission to write')
        self.version = version
        return True


    @staticmethod
    def _request(url, timeout=5):
        try:
            req = urllib2.Request(url, headers={
                'User-Agent': 'lel',
            })
            resp = urllib2.urlopen(req, timeout=timeout)
            html = resp.read()
            resp.close()
            return html, False
        except urllib2.HTTPError as e:
            return False, e.code
        except:
            return False, False
