#!/bin/env python2
import programs
import json
import getpass
from sys import exit, platform as _platform

if 'linux' not in _platform:
    print '[-] autocheck currently only supports linux'
    exit()

if getpass.getuser() != 'root':
    print '[-] autocheck required root. Exiting.'
    exit()


try:
    with open('config.json', 'r') as json_file:
        config = json.load(json_file)
except:
    print '[-] config.json not found. Exiting.'
    exit()

if config['pma']:
    pma = programs.PHPMyAdmin(config['pma_path'], config['pma_user'])
    if not config['delete_all_files_before_update']:
        pma.delete_all = False
    pma.check_update()

if config['pydio']:
    pydio = programs.Pydio(config['pydio_path'], config['pydio_user'])
    pydio.check_update()
