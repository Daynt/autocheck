# autocheck Changelog

## [0.0.1] (23-04-2015)
- Fixed compare version bug
- Fixed change user owner bug
- Check if linux is installed
- Added remove all files before updating setting
- Added MIT License